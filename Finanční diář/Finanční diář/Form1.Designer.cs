﻿namespace Finanční_diář
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.mainTab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.DatumCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlatceCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CisloUctuCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CastkaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.DatumCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrijemceCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CislouctuCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CastkaCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.obnovitButton = new System.Windows.Forms.Button();
            this.pridatButton = new System.Windows.Forms.Button();
            this.prioritaBox = new System.Windows.Forms.ListBox();
            this.castkaBox = new System.Windows.Forms.TextBox();
            this.cisloUctuBox = new System.Windows.Forms.TextBox();
            this.prijemceBox = new System.Windows.Forms.TextBox();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.dateLabel = new System.Windows.Forms.Label();
            this.termLabel = new System.Windows.Forms.Label();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.splatnostClm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.termPrijemceCml = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.termCisloUctuClm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.termCastkaClm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.termPrioritaClm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.nactiTerminy = new System.Windows.Forms.Button();
            this.souborTerminy = new System.Windows.Forms.Label();
            this.nactiVydaje = new System.Windows.Forms.Button();
            this.nactiPrijmy = new System.Windows.Forms.Button();
            this.souborVydaje = new System.Windows.Forms.Label();
            this.souborPrijmy = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.prioritaLabel = new System.Windows.Forms.Label();
            this.mainTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(80, 73);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Přidat příjem";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Datum:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(80, 109);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(200, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.Text = "Plátce:";
            this.textBox1.Click += new System.EventHandler(this.textboxClicked);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(80, 177);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(200, 20);
            this.textBox2.TabIndex = 5;
            this.textBox2.Text = "Částka:";
            this.textBox2.Click += new System.EventHandler(this.textboxClicked);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(80, 214);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Přidat";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // mainTab
            // 
            this.mainTab.Controls.Add(this.tabPage1);
            this.mainTab.Controls.Add(this.tabPage2);
            this.mainTab.Controls.Add(this.tabPage3);
            this.mainTab.Controls.Add(this.tabPage4);
            this.mainTab.Location = new System.Drawing.Point(0, 1);
            this.mainTab.Name = "mainTab";
            this.mainTab.SelectedIndex = 3;
            this.mainTab.Size = new System.Drawing.Size(786, 305);
            this.mainTab.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.textBox3);
            this.tabPage1.Controls.Add(this.dateTimePicker1);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.textBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(778, 279);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Přímy";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(282, 246);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 9;
            this.button4.Text = "Obnovit";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.click_obnovit);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DatumCol,
            this.PlatceCol,
            this.CisloUctuCol,
            this.CastkaCol});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(363, 15);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(405, 254);
            this.dataGridView1.TabIndex = 8;
            // 
            // DatumCol
            // 
            this.DatumCol.HeaderText = "Datum";
            this.DatumCol.Name = "DatumCol";
            this.DatumCol.ReadOnly = true;
            // 
            // PlatceCol
            // 
            this.PlatceCol.HeaderText = "Plátce";
            this.PlatceCol.Name = "PlatceCol";
            this.PlatceCol.ReadOnly = true;
            // 
            // CisloUctuCol
            // 
            this.CisloUctuCol.HeaderText = "Číslo účtu";
            this.CisloUctuCol.Name = "CisloUctuCol";
            this.CisloUctuCol.ReadOnly = true;
            // 
            // CastkaCol
            // 
            this.CastkaCol.HeaderText = "Částka";
            this.CastkaCol.Name = "CastkaCol";
            this.CastkaCol.ReadOnly = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(80, 142);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(200, 20);
            this.textBox3.TabIndex = 7;
            this.textBox3.Text = "Číslo účtu:";
            this.textBox3.Click += new System.EventHandler(this.textboxClicked);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.textBox6);
            this.tabPage2.Controls.Add(this.textBox5);
            this.tabPage2.Controls.Add(this.textBox4);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.dateTimePicker2);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(778, 279);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Výdaje";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(282, 246);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 8;
            this.button3.Text = "Obnovit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.click_obnovit);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DatumCol2,
            this.PrijemceCol,
            this.CislouctuCol2,
            this.CastkaCol2});
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(363, 15);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(405, 254);
            this.dataGridView2.TabIndex = 7;
            // 
            // DatumCol2
            // 
            this.DatumCol2.HeaderText = "Datum";
            this.DatumCol2.Name = "DatumCol2";
            this.DatumCol2.ReadOnly = true;
            // 
            // PrijemceCol
            // 
            this.PrijemceCol.HeaderText = "Příjemce";
            this.PrijemceCol.Name = "PrijemceCol";
            this.PrijemceCol.ReadOnly = true;
            // 
            // CislouctuCol2
            // 
            this.CislouctuCol2.HeaderText = "Číslo protiúčtu";
            this.CislouctuCol2.Name = "CislouctuCol2";
            this.CislouctuCol2.ReadOnly = true;
            // 
            // CastkaCol2
            // 
            this.CastkaCol2.HeaderText = "Částka";
            this.CastkaCol2.Name = "CastkaCol2";
            this.CastkaCol2.ReadOnly = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(80, 214);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Přidat";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(80, 177);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(200, 20);
            this.textBox6.TabIndex = 5;
            this.textBox6.Text = "Částka:";
            textBox6.Click += new System.EventHandler(this.textboxClicked);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(80, 142);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(200, 20);
            this.textBox5.TabIndex = 4;
            this.textBox5.Text = "Číslo protiúčtu:";
            textBox5.Click += new System.EventHandler(this.textboxClicked);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(80, 109);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(200, 20);
            this.textBox4.TabIndex = 3;
            this.textBox4.Text = "Příjemce:";
            textBox4.Click += new System.EventHandler(this.textboxClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Datum: ";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(80, 73);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Přidat výdaj";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.prioritaLabel);
            this.tabPage3.Controls.Add(this.obnovitButton);
            this.tabPage3.Controls.Add(this.pridatButton);
            this.tabPage3.Controls.Add(this.prioritaBox);
            this.tabPage3.Controls.Add(this.castkaBox);
            this.tabPage3.Controls.Add(this.cisloUctuBox);
            this.tabPage3.Controls.Add(this.prijemceBox);
            this.tabPage3.Controls.Add(this.dateTimePicker3);
            this.tabPage3.Controls.Add(this.dateLabel);
            this.tabPage3.Controls.Add(this.termLabel);
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(778, 279);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Termíny";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // obnovitButton
            // 
            this.obnovitButton.Location = new System.Drawing.Point(191, 246);
            this.obnovitButton.Name = "obnovitButton";
            this.obnovitButton.Size = new System.Drawing.Size(75, 23);
            this.obnovitButton.TabIndex = 16;
            this.obnovitButton.Text = "Obnovit";
            this.obnovitButton.UseVisualStyleBackColor = true;
            this.obnovitButton.Click += new System.EventHandler(this.click_obnovit);
            // 
            // pridatButton
            // 
            this.pridatButton.Location = new System.Drawing.Point(80, 246);
            this.pridatButton.Name = "pridatButton";
            this.pridatButton.Size = new System.Drawing.Size(75, 23);
            this.pridatButton.TabIndex = 15;
            this.pridatButton.Text = "Přidat";
            this.pridatButton.UseVisualStyleBackColor = true;
            this.pridatButton.Click += new System.EventHandler(this.pridatButton_Click);
            // 
            // prioritaBox
            // 
            this.prioritaBox.FormattingEnabled = true;
            this.prioritaBox.Location = new System.Drawing.Point(80, 213);
            this.prioritaBox.Name = "prioritaBox";
            this.prioritaBox.Size = new System.Drawing.Size(120, 30);
            this.prioritaBox.TabIndex = 14;
            // 
            // castkaBox
            // 
            this.castkaBox.Location = new System.Drawing.Point(80, 177);
            this.castkaBox.Name = "castkaBox";
            this.castkaBox.Size = new System.Drawing.Size(172, 20);
            this.castkaBox.TabIndex = 13;
            this.castkaBox.Text = "Částka";
            castkaBox.Click += new System.EventHandler(this.textboxClicked);
            // 
            // cisloUctuBox
            // 
            this.cisloUctuBox.Location = new System.Drawing.Point(80, 142);
            this.cisloUctuBox.Name = "cisloUctuBox";
            this.cisloUctuBox.Size = new System.Drawing.Size(173, 20);
            this.cisloUctuBox.TabIndex = 12;
            this.cisloUctuBox.Text = "Číslo účtu";
            cisloUctuBox.Click += new System.EventHandler(this.textboxClicked);
            // 
            // prijemceBox
            // 
            this.prijemceBox.Location = new System.Drawing.Point(80, 109);
            this.prijemceBox.Name = "prijemceBox";
            this.prijemceBox.Size = new System.Drawing.Size(172, 20);
            this.prijemceBox.TabIndex = 11;
            this.prijemceBox.Text = "Příjemce";
            prijemceBox.Click += new System.EventHandler(this.textboxClicked);
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Location = new System.Drawing.Point(80, 73);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(172, 20);
            this.dateTimePicker3.TabIndex = 10;
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Location = new System.Drawing.Point(51, 46);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(54, 13);
            this.dateLabel.TabIndex = 9;
            this.dateLabel.Text = "Splatnost:";
            // 
            // termLabel
            // 
            this.termLabel.AutoSize = true;
            this.termLabel.Location = new System.Drawing.Point(24, 15);
            this.termLabel.Name = "termLabel";
            this.termLabel.Size = new System.Drawing.Size(68, 13);
            this.termLabel.TabIndex = 8;
            this.termLabel.Text = "Přidat termín";
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.splatnostClm,
            this.termPrijemceCml,
            this.termCisloUctuClm,
            this.termCastkaClm,
            this.termPrioritaClm});
            this.dataGridView3.EnableHeadersVisualStyles = false;
            this.dataGridView3.Location = new System.Drawing.Point(272, 3);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.Size = new System.Drawing.Size(503, 273);
            this.dataGridView3.TabIndex = 7;
            // 
            // splatnostClm
            // 
            this.splatnostClm.HeaderText = "Splatnost";
            this.splatnostClm.Name = "splatnostClm";
            this.splatnostClm.ReadOnly = true;
            // 
            // termPrijemceCml
            // 
            this.termPrijemceCml.HeaderText = "Příjemce";
            this.termPrijemceCml.Name = "termPrijemceCml";
            this.termPrijemceCml.ReadOnly = true;
            // 
            // termCisloUctuClm
            // 
            this.termCisloUctuClm.HeaderText = "Číslo účtu";
            this.termCisloUctuClm.Name = "termCisloUctuClm";
            this.termCisloUctuClm.ReadOnly = true;
            // 
            // termCastkaClm
            // 
            this.termCastkaClm.HeaderText = "Částka";
            this.termCastkaClm.Name = "termCastkaClm";
            this.termCastkaClm.ReadOnly = true;
            // 
            // termPrioritaClm
            // 
            this.termPrioritaClm.HeaderText = "Priorita";
            this.termPrioritaClm.Name = "termPrioritaClm";
            this.termPrioritaClm.ReadOnly = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.nactiTerminy);
            this.tabPage4.Controls.Add(this.souborTerminy);
            this.tabPage4.Controls.Add(this.nactiVydaje);
            this.tabPage4.Controls.Add(this.nactiPrijmy);
            this.tabPage4.Controls.Add(this.souborVydaje);
            this.tabPage4.Controls.Add(this.souborPrijmy);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(778, 279);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Nastavení";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // nactiTerminy
            // 
            this.nactiTerminy.Location = new System.Drawing.Point(21, 208);
            this.nactiTerminy.Name = "nactiTerminy";
            this.nactiTerminy.Size = new System.Drawing.Size(92, 23);
            this.nactiTerminy.TabIndex = 5;
            this.nactiTerminy.Text = "Soubor termíny";
            this.nactiTerminy.UseVisualStyleBackColor = true;
            this.nactiTerminy.Click += new System.EventHandler(this.terminyButton_Click);
            // 
            // souborTerminy
            // 
            this.souborTerminy.AutoSize = true;
            this.souborTerminy.Location = new System.Drawing.Point(18, 79);
            this.souborTerminy.Name = "souborTerminy";
            this.souborTerminy.Size = new System.Drawing.Size(229, 13);
            this.souborTerminy.TabIndex = 4;
            this.souborTerminy.Text = "Nevybrán žáden soubor pro položky \"Termíny\"";
            // 
            // nactiVydaje
            // 
            this.nactiVydaje.Location = new System.Drawing.Point(21, 169);
            this.nactiVydaje.Name = "nactiVydaje";
            this.nactiVydaje.Size = new System.Drawing.Size(92, 23);
            this.nactiVydaje.TabIndex = 3;
            this.nactiVydaje.Text = "Soubor výdaje";
            this.nactiVydaje.UseVisualStyleBackColor = true;
            this.nactiVydaje.Click += new System.EventHandler(this.nactiVydaje_Click);
            // 
            // nactiPrijmy
            // 
            this.nactiPrijmy.Location = new System.Drawing.Point(21, 130);
            this.nactiPrijmy.Name = "nactiPrijmy";
            this.nactiPrijmy.Size = new System.Drawing.Size(92, 23);
            this.nactiPrijmy.TabIndex = 2;
            this.nactiPrijmy.Text = "Soubor příjmy";
            this.nactiPrijmy.UseVisualStyleBackColor = true;
            this.nactiPrijmy.Click += new System.EventHandler(this.nactiPrijmy_Click);
            // 
            // souborVydaje
            // 
            this.souborVydaje.AutoSize = true;
            this.souborVydaje.Location = new System.Drawing.Point(18, 47);
            this.souborVydaje.Name = "souborVydaje";
            this.souborVydaje.Size = new System.Drawing.Size(216, 13);
            this.souborVydaje.TabIndex = 1;
            this.souborVydaje.Text = "Nevybrán žáden soubor pro položky \'Výdaje\'";
            // 
            // souborPrijmy
            // 
            this.souborPrijmy.AutoSize = true;
            this.souborPrijmy.Location = new System.Drawing.Point(18, 15);
            this.souborPrijmy.Name = "souborPrijmy";
            this.souborPrijmy.Size = new System.Drawing.Size(214, 13);
            this.souborPrijmy.TabIndex = 0;
            this.souborPrijmy.Text = "Nevybrán žáden soubor pro položky \'Příjmy\'";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // prioritaLabel
            // 
            this.prioritaLabel.AutoSize = true;
            this.prioritaLabel.Location = new System.Drawing.Point(8, 221);
            this.prioritaLabel.Name = "prioritaLabel";
            this.prioritaLabel.Size = new System.Drawing.Size(42, 13);
            this.prioritaLabel.TabIndex = 17;
            this.prioritaLabel.Text = "Priorita:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 304);
            this.Controls.Add(this.mainTab);
            this.Name = "Form1";
            this.Text = "Form1";
            this.mainTab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl mainTab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlatceCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CisloUctuCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CastkaCol;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrijemceCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CislouctuCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CastkaCol2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button nactiVydaje;
        private System.Windows.Forms.Button nactiPrijmy;
        private System.Windows.Forms.Label souborVydaje;
        private System.Windows.Forms.Label souborPrijmy;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridViewTextBoxColumn splatnostClm;
        private System.Windows.Forms.DataGridViewTextBoxColumn termPrijemceCml;
        private System.Windows.Forms.DataGridViewTextBoxColumn termCisloUctuClm;
        private System.Windows.Forms.DataGridViewTextBoxColumn termCastkaClm;
        private System.Windows.Forms.DataGridViewTextBoxColumn termPrioritaClm;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.Label termLabel;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.ListBox prioritaBox;
        private System.Windows.Forms.TextBox castkaBox;
        private System.Windows.Forms.TextBox cisloUctuBox;
        private System.Windows.Forms.TextBox prijemceBox;
        private System.Windows.Forms.Button obnovitButton;
        private System.Windows.Forms.Button pridatButton;
        private System.Windows.Forms.Button nactiTerminy;
        private System.Windows.Forms.Label souborTerminy;
        private System.Windows.Forms.Label prioritaLabel;
    }
}

