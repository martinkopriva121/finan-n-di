﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Finanční_diář
{
    public partial class Form1 : Form
    {
        private XMLWorker.Writer xmlwriter = null;
        private XMLWorker.Reader xmlreader = null;
        public Form1()
        {
            InitializeComponent();

            LoadPrioritaBox();

            souborPrijmy.Text = "Nevybrán žáden soubor pro položky 'Příjmy'";
            souborVydaje.Text = "Nevybrán žáden soubor pro položky 'Výdaje'";

            xmlwriter = new XMLWorker.Writer();
            xmlreader = new XMLWorker.Reader();
        }

        private void LoadPrioritaBox()
        {
            List<string> items = new List<string>();

            items.Add("Nízká");
            items.Add("Střední");
            items.Add("Vysoká");

            prioritaBox.DataSource = items;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string datum = dateTimePicker1.Text;
            string platce = textBox1.Text;
            string castka = textBox2.Text;
            string cisloUctu = textBox3.Text;

            xmlwriter.novyPrijem(datum, platce, castka, cisloUctu, souborPrijmy.Text);
        }

        private void textboxClicked(object sender, EventArgs e)
        {
            if (sender == textBox1)
                textBox1.Clear();
            else if (sender == textBox2)
                textBox2.Clear();
            else if (sender == textBox3)
                textBox3.Clear();
            else if (sender == textBox4)
                textBox4.Clear();
            else if (sender == textBox5)
                textBox5.Clear();
            else if (sender == textBox6)
                textBox6.Clear();
            else if (sender == prijemceBox)
                prijemceBox.Clear();
            else if (sender == cisloUctuBox)
                cisloUctuBox.Clear();
            else if (sender == castkaBox)
                castkaBox.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string datum = dateTimePicker2.Text;
            string prijemce = textBox4.Text;
            string castka = textBox6.Text;
            string cisloProtiuctu = textBox5.Text;

            xmlwriter.novyVydaj(datum, prijemce, castka, cisloProtiuctu, souborVydaje.Text);
        }

        private void click_obnovit(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            dataGridView3.Rows.Clear();

            xmlreader.nactiPrijmy(dataGridView1, souborPrijmy.Text);
            xmlreader.nactiVydaje(dataGridView2, souborVydaje.Text);
            xmlreader.nactiTerminy(dataGridView3, souborTerminy.Text);
        }

        private void nactiPrijmy_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "XML Files|*.xml";

            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
                souborPrijmy.Text = openFileDialog1.FileName;
        }

        private void nactiVydaje_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "XML Files|*.xml";

            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
                souborVydaje.Text = openFileDialog1.FileName;
        }

        private void pridatButton_Click(object sender, EventArgs e)
        {
            string splatnost = dateTimePicker3.Text;
            string prijemce = prijemceBox.Text;
            string castka = castkaBox.Text;
            string cisloProtiuctu = cisloUctuBox.Text;
            string priorita = prioritaBox.Text;

            xmlwriter.novyTermin(splatnost, prijemce, cisloProtiuctu, castka, priorita, souborTerminy.Text);
        }

        private void terminyButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "XML Files|*.xml";

            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
                souborTerminy.Text = openFileDialog1.FileName;
        }
    }
}
