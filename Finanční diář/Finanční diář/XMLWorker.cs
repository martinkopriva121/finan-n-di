﻿
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Finanční_diář
{
    public class XMLWorker
    {
        public class Writer
        {
            public void novyPrijem(string datum, string platce, string castka, string cislouctu, string fileName)
            {
                var prijem = new XElement("Prijem", new XElement("Datum", datum), new XElement("Platce", platce), new XElement("Castka", castka), new XElement("Cislouctu", cislouctu));
                var document = new XDocument();

                if (File.Exists(fileName))
                {
                    document = XDocument.Load(fileName);
                    document.Element("Prijmy").Add(prijem);
                    document.Save(fileName);
                }
                else
                {
                    document = new XDocument(new XElement("Prijmy", prijem));
                    document.Save("prijmy.xml");
                }
            }

            public void novyVydaj(string datum, string prijemce, string castka, string cislouctu, string fileName)
            {
                var vydaj = new XElement("Vydaj", new XElement("Datum", datum), new XElement("Prijemce", prijemce), new XElement("Castka", castka), new XElement("Cislouctu", cislouctu));
                var document = new XDocument();

                if (File.Exists(fileName))
                {
                    document = XDocument.Load(fileName);
                    document.Element("Vydaje").Add(vydaj);
                    document.Save(fileName);
                }
                else
                {
                    document = new XDocument(new XElement("Vydaje", vydaj));
                    document.Save("vydaje.xml");
                }
            }

            public void novyTermin(string splatnost, string prijemce, string cislouctu, string castka, string priorita, string fileName)
            {
                var termin = new XElement("Termin", new XElement("Splatnost", splatnost), new XElement("Prijemce", prijemce), new XElement("Cislouctu", cislouctu), 
                    new XElement("Castka", castka), new XElement("Priorita", priorita));
                var document = new XDocument();

                if (File.Exists(fileName))
                {
                    document = XDocument.Load(fileName);
                    document.Element("Terminy").Add(termin);
                    document.Save(fileName);
                }
                else
                {
                    document = new XDocument(new XElement("Terminy", termin));
                    document.Save("terminy.xml");
                }
            }
        }

        public class Reader
        {
            public void nactiPrijmy(DataGridView dgv, string fileName)
            {
                if (!File.Exists(fileName))
                {
                    MessageBox.Show("Soubor .xml s daty o příjmech neexistuje!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                XDocument document = XDocument.Load(fileName);

                int count = document.Descendants("Prijem").Count();

                var selected = from r in document.Descendants("Prijem")
                                  select r;

                for (int i = 0; i <= count -1; i++)
                {
                    XElement a = selected.ElementAt(i);

                    string[] row = new string[] { a.Element("Datum").Value, a.Element("Platce").Value, a.Element("Cislouctu").Value, a.Element("Castka").Value + " Kč" };
                    dgv.Rows.Add(row);
                }
            }

            public void nactiVydaje(DataGridView dgv, string fileName)
            {
                if (!File.Exists(fileName))
                {
                    MessageBox.Show("Soubor .xml s daty o výdajích neexistuje!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                XDocument document = XDocument.Load(fileName);

                int count = document.Descendants("Vydaj").Count();

                var selected = from r in document.Descendants("Vydaj")
                               select r;

                for (int i = 0; i <= count - 1; i++)
                {
                    XElement a = selected.ElementAt(i);

                    string[] row = new string[] { a.Element("Datum").Value, a.Element("Prijemce").Value, a.Element("Cislouctu").Value, a.Element("Castka").Value + " Kč" };
                    dgv.Rows.Add(row);
                }
            }

            public void nactiTerminy(DataGridView dvg, string fileName)
            {
                if (!File.Exists(fileName))
                {
                    MessageBox.Show("Soubor .xml s termíny plateb neexistuje!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                XDocument document = XDocument.Load(fileName);

                int count = document.Descendants("Termin").Count();

                var selected = from r in document.Descendants("Termin")
                               select r;

                for (int i = 0; i <= count - 1; i++)
                {
                    XElement a = selected.ElementAt(i);

                    string[] row = new string[] { a.Element("Splatnost").Value, a.Element("Prijemce").Value, a.Element("Cislouctu").Value, a.Element("Castka").Value + " Kč", a.Element("Priorita").Value };
                    dvg.Rows.Add(row);
                }
            }
        }
    }
}
